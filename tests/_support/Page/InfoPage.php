<?php
namespace Page;

class InfoPage
{
    // include url of current page
    public static $URL = '/';

    //elements
    const LOGO = '//*[@class="logo-img"]';
    const INFO_CONTAINER = '//*[@class="col-sm-12 col-md-12"]';
    const REGISTER_FORM = '//*[@id="register-form"]/div';
    const GALLERY = '//*[@class="container booking-gallery"]';
    const EMPLOYEE = '//*[@class="row show-by-toggle"]';
    const INFO_BLOCK = '//*[@class="tabs-accordion"]';
    const FOOTER = '//*[@id="footer"]';
    const FOOTER_SOCIAL = '//*[@class="copyright col-xs-12 col-sm-3 col-md-3 social"]';
    public static $COPYRIGHT = 'Copyright © zags.by';
    const FOOTER_SOCIAL_VK = '//*[@class="icon rounded color icon-vk"]';
    public static $VK_TITLE = 'ZAGS.by СВАДЬБА МОГИЛЁВ, ГОМЕЛЬ, ВИТЕБСК, МИНСК';
    const FOOTER_SOCIAL_OK = '//*[@class="icon rounded color icon-odnoklassniki"]';
    public static $OK_TITLE = 'ZAGS.by Всё о свадьбе в Могилёве! И не только...';
    const FOOTER_SOCIAL_FB = '//*[@class="icon rounded color icon-facebook"]';
    public static $FB_TITLE = 'ZAGS.by';
    const FOOTER_PHONE = '//*[@class="phone col-xs-12 col-sm-3 col-md-3"]';
    const FOOTER_PROJECT_LINKS_BLOCK = '//*[@class="phone col-xs-6 col-sm-3 col-md-2"]';
    const FOOTER_ADDRESS = '//*[@class="phone col-xs-6 col-sm-3 col-md-3"]';



    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

    public static function of(\AcceptanceTester $I)
    {
        return new static($I);
    }

    public static function route($param)
    {
        return static::$URL.$param;
    }


}
