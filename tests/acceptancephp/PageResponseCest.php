<?php

use Page\InfoPage;
use Page\MogilevPage;
use Page\BobruiskPage;
use Page\OsipovichiPage;
use Page\GorkiPage;
use Page\KrugloePage;
use Page\KrichevPage;
use Page\ShklovPage;
use Page\KostukovichiPage;
use Page\KlimovichiPage;
use Page\MstislavlPage;
use Page\ChausyPage;
use Page\BelynichiPage;
use Page\KirovskPage;
use Page\CherikovPage;
use Page\SlavgorodPage;
use Page\GluskPage;
use Page\KlichevPage;
use Page\DribinPage;
use Page\KrasnopoliePage;
use Page\HotimskPage;
use Page\ByhovPage;

class PageResponseCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->amOnPage(InfoPage::$URL);
        $I->canSeeResponseCodeIs('200');
        //могилевская область
        $I->amOnUrl(MogilevPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(BobruiskPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(OsipovichiPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(GorkiPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KrugloePage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KrichevPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(ShklovPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KostukovichiPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KlimovichiPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(MstislavlPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(ChausyPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(BelynichiPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KirovskPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(CherikovPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(SlavgorodPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(GluskPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KlichevPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(DribinPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(KrasnopoliePage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(HotimskPage::$URL);
        $I->canSeeResponseCodeIs('200');

        $I->amOnUrl(ByhovPage::$URL);
        $I->canSeeResponseCodeIs('200');
    }
}
