<?php
use Page\InfoPage;
use Page\ByhovPage;

class ByhovPageCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->amOnUrl(ByhovPage::$URL);
        $I->canSeeElement(InfoPage::LOGO);
        $I->canSeeElement(InfoPage::INFO_CONTAINER);
        $I->canSeeElement(InfoPage::EMPLOYEE);
        $I->canSeeElement(InfoPage::INFO_BLOCK);
        $I->canSeeElement(InfoPage::FOOTER);
        $I->canSeeElement(InfoPage::FOOTER_SOCIAL);
        $I->canSee(InfoPage::$COPYRIGHT,InfoPage::FOOTER_SOCIAL);
        $I->canSeeElement(InfoPage::FOOTER_SOCIAL_VK);
        $I->click(InfoPage::FOOTER_SOCIAL_VK);
        $I->switchToNextTab();
        $I->canSee(InfoPage::$VK_TITLE);
        $I->closeTab();
        $I->canSeeElement(InfoPage::FOOTER_SOCIAL_OK);
        $I->click(InfoPage::FOOTER_SOCIAL_OK);
        $I->switchToNextTab();
        $I->canSee(InfoPage::$OK_TITLE);
        $I->closeTab();
        $I->canSeeElement(InfoPage::FOOTER_SOCIAL_FB);
        $I->click(InfoPage::FOOTER_SOCIAL_FB);
        $I->switchToNextTab();
        $I->canSee(InfoPage::$FB_TITLE);
        $I->closeTab();
        $I->canSeeElement(InfoPage::FOOTER_PHONE);
        $I->canSeeElement(InfoPage::FOOTER_PROJECT_LINKS_BLOCK);
        $I->canSeeElement(InfoPage::FOOTER_ADDRESS);


    }
}
